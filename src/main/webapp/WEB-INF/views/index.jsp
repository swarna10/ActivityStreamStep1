<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page session="false" isELIgnored="false" %>
<html>
<head>

<title>ActivityStreamStep_1 - Messanger</title>
</head>
<body>
	
<!-- create a form which will have textboxes for Sender Name and Message content along with a Send 
Submit button. Handle errors like empty fields -->
	
	
	
<!-- display all existing messages in a tabular structure with Sender Name, Posted Date and Message -->	


<form action="sendMessage" method="Post">
		<table>
			<tr>
				<td>Sender Name</td>
				<td><input type="text" name="sender"></td>
			</tr>

			<tr>
				<td>Message</td>
				<td><input type="text" name="message"></td>
			</tr>
			<tr>

				<td><input type="submit" value="send">
			</tr>
		</table>

	</form>


<table>
			<tr>
				<td>Sender Name</td>
				<td>Message</td>
				<td>PostedDate</td>				
			</tr>
			<c:forEach items="${messages}" var="message" >
			<tr>
				<td>${msg.senderName}</td>
				<td>${msg.message}</td>
				<td>${msg.postedDate}</td>		
			</tr>
			</c:forEach>

		</table>
</body>
</html>