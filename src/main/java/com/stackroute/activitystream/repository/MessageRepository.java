package com.stackroute.activitystream.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.stackroute.activitystream.config.HibernateUtil;
import com.stackroute.activitystream.model.Message;

/*
 * This class contains the code for database interactions and methods 
 * of this class will be used by other parts of the applications such
 * as Controllers and Test Cases
 * */
public class MessageRepository {
	SessionFactory sessionFactory;
	Session session;

	public MessageRepository() {
		/*
		 * create a hibernate session from HibernateUtil
		 * 
		 */
		sessionFactory =HibernateUtil.getSessionFactory();
	}
	
	/*
	 * This method is used to save messages in database
	 */ 
	public boolean sendMessage(Message message) {
		
		session= sessionFactory.openSession();
		session.beginTransaction();
		session.save(message);
		session.getTransaction().commit();
		return true;
	}
	
	/*
	 * This method is used to retrieve all messages in database
	 */
	public List<Message> getAllMessages(){
		
		session =sessionFactory.openSession();
		return session.createQuery("from Message  order by postedDate").list();
	}
}
